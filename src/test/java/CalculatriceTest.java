import org.junit.Assert;
import org.junit.Test;

public class CalculatriceTest {
    Calculatrice c = new Calculatrice();

    @Test
    public void additionTest(){
        Assert.assertEquals(5,c.addition(3,2));
    }
    @Test
    public void soustractionTest(){
        Assert.assertEquals(2,c.soustraction(6,4));
    }
    @Test
    public void divisionTest(){
        Assert.assertEquals(2,c.division(4,2));
    }
    @Test
    public void multiplicationTest(){
        Assert.assertEquals(9,c.mutliplication(3,3));
    }
}
